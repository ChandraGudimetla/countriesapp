package com.example.androidlatestcomponents.di;

import com.example.androidlatestcomponents.model.CountriesApi;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    private static final String BASE_URL = "https://raw.githubusercontent.com";

    @Provides
    public CountriesApi provideCountriesApi(){
        return new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()) //here we get the json in the form of a java object (list of country types)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) //takes the list and converts it into a rxjava component as we defined as single observable
            .build()
            .create(CountriesApi.class);
    }

}

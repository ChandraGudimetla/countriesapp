package com.example.androidlatestcomponents.di;

import com.example.androidlatestcomponents.model.CountriesService;

import dagger.Component;

@Component(modules = {ApiModule.class})
public interface ApiComponent {

    void inject(CountriesService service);
}

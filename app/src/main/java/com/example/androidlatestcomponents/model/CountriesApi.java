package com.example.androidlatestcomponents.model;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface CountriesApi {

        /*
    Single - Type of observable that RX java generates for us. Its an observable that emits only one value and finishes
    ex: LiveData is also observable
    Here single will return the list of countries
    * */

    @GET("DevTides/countries/master/countriesV2.json")
    Single<List<CountryModel>> getCountries();

}

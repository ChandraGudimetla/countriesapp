package com.example.androidlatestcomponents.model;


import com.example.androidlatestcomponents.di.DaggerApiComponent;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;


//This is a singleton class
public class CountriesService {



    //Standard way of creating singleton class make a private variable instance
    private static CountriesService instance;

    @Inject
    public CountriesApi api;

    private CountriesService(){
        DaggerApiComponent.create().inject(this);
    }

    public static CountriesService getInstance(){
        if (instance == null){
            instance = new CountriesService();
        }
        return instance;
    }

    public Single<List<CountryModel>> getCountries(){
        return api.getCountries();
    }
}



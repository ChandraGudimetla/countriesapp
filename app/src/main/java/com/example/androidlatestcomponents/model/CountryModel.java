package com.example.androidlatestcomponents.model;

import com.google.gson.annotations.SerializedName;

public class CountryModel {
    @SerializedName("name")
    String countryName;
    @SerializedName("capital")
    String capital;

    //flag will be an url which will have the flag of the country
    @SerializedName("flagPNG")
    String flag;

    //constructor
    public CountryModel(String countryName, String capital, String flag) {
        this.countryName = countryName;
        this.capital = capital;
        this.flag = flag;
    }

    /*The reason why we have getters are by default in java if we do not specify any access specifier for variables
    than those variables are protected and in order to expose we have getters*/

    public String getCountryName() {
        return countryName;
    }

    public String getCapital() {
        return capital;
    }

    public String getFlag() {
        return flag;
    }
}
